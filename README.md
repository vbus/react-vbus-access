# react-vbus-access

A React components library for vBus.

## Installation

Install `@veea/react-vbus-access`:

    npm install --save bitbucket:vbus/react-vbus-access

## Documentation

    npm i
    npm run doc


## Setup Context

```typescript
import {LocalStorage, Client} from "@veea/vbus-access";
import {VbusAccessContext} from "@veea/react-vbus-access"

const App = () => {
    return (
        <VbusAccessContext.Provider>
            <Home/>
        </VbusAccessContext.Provider>
    )
}
```
