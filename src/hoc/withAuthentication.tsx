import React from "react";
import AuthenticatedBoundary from "../components/AuthenticatedBoundary";

const withAuthentication = <P extends object>(
    Component: React.ComponentType<P>,
    fallback: React.ReactNode,
    loading: React.ReactNode,
) =>
    class WithAuthentication extends React.Component<P> {
        render() {
            return (
                <AuthenticatedBoundary fallback={fallback} loading={loading}>
                    <Component {...this.props}/>
                </AuthenticatedBoundary>
            )
        }
    };

export default withAuthentication
