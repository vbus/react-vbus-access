import React, {useContext} from "react";
import {ApiContext} from "../VbusAccessContext";

/**
 * Retrieve an attribute value.
 * @param path Vbus path like system.info.attribute
 */
export default function useAttributeValue<T = any>(path: string): { value: T | null, error: Error | null } {
    const client = useContext(ApiContext)
    if (client === null) {
        throw new Error("useAttributeValue should be used inside an ApiContext")
    }

    const [data, setData] = React.useState<T | null>(null)
    const [error, setError] = React.useState<Error | null>(null);

    const fetchData = async () => {
        try {
            const attr = await client.getRemoteAttr(...path.split("."))
            if (!attr) {
                setError(new Error("cannot retrieve remote attribute"))
                return
            }

            const value = await attr.readValue()
            setData(value);
        } catch (error) {
            setError(error);
        }
    };

    React.useEffect(() => {
        fetchData();
    }, []);

    return {value: data, error};
}
