import React, {useContext} from "react";
import {ApiContext} from "../VbusAccessContext";


/**
 * A hook to retrieve the authentication status.
 * A null indicate a loading state.
 */
export default function useIsAuthenticated(): boolean | null {
    const client = useContext(ApiContext)
    if (client === null) {
        throw new Error("useIsAuthenticated should be used inside an ApiContext")
    }

    const [isAuthenticated, setIsAuthenticated] = React.useState<boolean | null>(null)

    // async job to check if we are authenticated
    const asyncJob = async () => {
        setIsAuthenticated(await client.isAuthenticated());
    };

    // called when storage is updated
    const onStorageUpdated = () => {
        asyncJob();
    }

    // listen for storage
    React.useEffect(() => {
        client.addAuthListener(onStorageUpdated)
        return () => {
            client.removeAuthListener(onStorageUpdated)
        };
    }, [client]);

    // check is authenticated once on render
    React.useEffect(() => {
        asyncJob();
    })

    return isAuthenticated;
}
