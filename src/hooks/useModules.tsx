import React, {useContext} from "react";
import {ModuleInfo} from "@veea/vbus-access";
import {ApiContext} from "../VbusAccessContext";


export default function useModules(timeout: number = 1): { modules: ModuleInfo[] | null, error: Error | null } {
    const client = useContext(ApiContext)
    if (client === null) {
        throw new Error("useModules should be used inside an ApiContext")
    }

    const [data, setData] = React.useState<ModuleInfo[] | null>(null)
    const [error, setError] = React.useState<Error | null>(null);

    const fetchData = async () => {
        try {
            const res = await client.discoverModules(timeout)
            setData(res);
        } catch (error) {
            setError(error);
        }
    };

    React.useEffect(() => {
        fetchData();
    }, []);

    return {modules: data, error};
};

