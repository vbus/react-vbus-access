import React, {useContext} from "react";
import {ApiContext} from "../VbusAccessContext";
import {UnknownProxy} from "@veea/vbus-access";


export default function useDiscover(path: string, timeout: number = 1): { proxy: UnknownProxy | null, error: Error | null } {
    const client = useContext(ApiContext)
    if (client === null) {
        throw new Error("useDiscover should be used inside an ApiContext")
    }

    const [data, setData] = React.useState<UnknownProxy | null>(null)
    const [error, setError] = React.useState<Error | null>(null);

    const fetchData = async () => {
        try {
            const res = await client.discover(path, timeout)
            setData(res);
        } catch (error) {
            setError(error);
        }
    };

    React.useEffect(() => {
        fetchData();
    }, []);

    return {proxy: data, error};
};

