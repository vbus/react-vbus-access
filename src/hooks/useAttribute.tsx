import React, {useContext} from "react";
import {ApiContext} from "../VbusAccessContext";
import {AttributeProxy} from "@veea/vbus-access";

/**
 * A hook to retrieve an attribute.
 * @param path Vbus path like system.info.attribute
 */
export default function useAttribute(path: string): { attr: AttributeProxy | null, error: Error | null } {
    const client = useContext(ApiContext)
    if (client === null) {
        throw new Error("useAttribute should be used inside an ApiContext")
    }

    const [attr, setAttr] = React.useState<AttributeProxy | null>(null)
    const [error, setError] = React.useState<Error | null>(null);

    const fetchData = async () => {
        try {
            const attr = await client.getRemoteAttr(...path.split("."))
            if (!attr) {
                setError(new Error("cannot retrieve remote attribute"))
                return
            }

            setAttr(attr);
        } catch (error) {
            setError(error);
        }
    };

    React.useEffect(() => {
        fetchData();
    }, []);

    return {attr: attr, error};
}
