import React, {useContext} from "react";
import {ApiContext} from "../VbusAccessContext";
import {MethodProxy} from "@veea/vbus-access";

/**
 * A hook to retrieve a method proxy.
 * @param path Vbus path like system.info.attribute
 */
export default function useMethod(path: string): { meth: MethodProxy | null, error: Error | null } {
    const client = useContext(ApiContext)
    if (client === null) {
        throw new Error("useMethod should be used inside an ApiContext")
    }

    const [meth, setMeth] = React.useState<MethodProxy | null>(null)
    const [error, setError] = React.useState<Error | null>(null);

    const fetchData = async () => {
        try {
            const meth = await client.getRemoteMethod(...path.split("."))
            if (!meth) {
                setError(new Error("cannot retrieve remote method"))
                return
            }

            setMeth(meth);
        } catch (error) {
            setError(error);
        }
    };

    React.useEffect(() => {
        fetchData();
    }, []);

    return {meth: meth, error};
}
