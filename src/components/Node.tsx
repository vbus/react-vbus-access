import React from "react";


export interface NodeContextValue {
    /**
     * A vBus path
     */
    path: string
}

export const NodeContext = React.createContext<NodeContextValue | null>(null)

interface NodeProps {
    children: React.ReactNode
    path: string
}

export  default function Node(props: NodeProps) {
    return (
        <NodeContext.Provider value={{path: props.path}}>
            {props.children}
        </NodeContext.Provider>
    )
}
