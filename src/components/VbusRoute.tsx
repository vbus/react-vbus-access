import React, {useContext} from "react";
import {Route, RouteProps} from "react-router-dom";
import {ApiContext} from "../VbusAccessContext";
import parse from "url-parse"

interface VbusRouteProps extends RouteProps {
    home?: boolean
}

/**
 * A extended Route component that work inside Veea dashboard iframe.
 */
export default function VbusRoute(props: VbusRouteProps) {
    const client = useContext(ApiContext)
    if (client === null) {
        throw new Error("Route should be used inside an ApiContext")
    }

    // http://localhost:8080/api/v1/static/system/mimik/boolangery-ThinkPad-P1-Gen-2
    const url = parse(client.baseUrl);
    const pathname = url.pathname;

    const {path, home, children, ...other} = props;

    return <>
        <Route path={path} {...other}>{children}</Route>
        <Route path={pathname + path} {...other}>{children}</Route>
        {home && <Route path={pathname + '/index.html'} {...other}>{children}</Route>}
    </>
}
