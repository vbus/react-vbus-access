import React from "react";
import useIsAuthenticated from "../hooks/useIsAuthenticated"

/**
 * A boundary component to render children only if the user is authenticated.
 *
 * @component
 */
export default function AuthenticatedBoundary(
    {children, fallback, loading}: {
        children: React.ReactNode,
        fallback: React.ReactNode,
        loading: React.ReactNode,
    }): JSX.Element {
    const isAuthenticated = useIsAuthenticated()
    return <>{isAuthenticated === null ? loading : (isAuthenticated ? children : fallback)}</>
}
