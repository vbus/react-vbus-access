import React, {useContext} from "react";
import {NodeContext} from "./Node";
import useAttributeValue from "../hooks/useAttributeValue";


/**
 * Props
 */
export interface AttributeProps<T> {
    /**
     * If value and error is null, then its loading.
     */
    children: (value: T | null, error: Error | null) => React.ReactNode,

    /**
     * A vBus path relative to surrounding Node
     */
    path: string
}

function AttributeValue<T = any>(props: AttributeProps<T>) {
    const nodeCtx = useContext(NodeContext);
    if (nodeCtx == null) {
        throw new Error("AttributeValue must be rendered inside a Node component")
    }

    const {value, error} = useAttributeValue<T>([nodeCtx.path, props.path].join("."))

    return (
        <>
            {props.children(value, error)}
        </>
    )
}

export default AttributeValue

