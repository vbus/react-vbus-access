package main

import (
	"fmt"

	"bitbucket.org/vbus/vbus.go"
	"github.com/sirupsen/logrus"
)

func main() {
    logrus.SetLevel(logrus.DebugLevel) // vbus library use logrus logging framework

	client := vBus.NewClient("system", "reactTemplate", vBus.WithStaticPath("./build"))
	if err := client.Connect(); err != nil {
		fmt.Print(err)
	}

	defer func() {
		if err := client.Close(); err != nil {
			fmt.Print(err)
		}
	}()
	select {}
}
