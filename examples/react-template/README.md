This is an example project for react-vbus-access library.

# Testing

To serve the UI with vBus Http server you must:

1. Build static files

```
    npm i
    npm run build
```

2. Expose those static files with the vBus library (example in main.go):

```
    go build main.go
    ./main    
```
