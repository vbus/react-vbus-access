module info

go 1.13

require (
	bitbucket.org/vbus/vbus.go v1.1.1-0.20200618075340-c50472b330e6
	github.com/pkg/errors v0.9.1 // indirect
	github.com/sirupsen/logrus v1.5.0
	github.com/stretchr/testify v1.5.1 // indirect
	golang.org/x/crypto v0.0.0-20200323165209-0ec3e9974c59 // indirect
	golang.org/x/sys v0.0.0-20200223170610-d5e6a3e2c0ae // indirect
	gopkg.in/yaml.v2 v2.2.4 // indirect
)
