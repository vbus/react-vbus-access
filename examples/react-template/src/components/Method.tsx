import React, {useState} from "react";
import {AttributeProxy, MethodProxy, UnknownProxy} from "@veea/vbus-access";
import {Button, Card, Form, Row, Col, Alert} from "react-bootstrap";


/**
 * Render a vBus method.
 */
export default function Method({meth}: { meth: MethodProxy }) {
    const [args, setArgs] = useState<any>("[]")
    const [retVal, setRetVal] = useState(null)
    const [error, setError] = useState(null)

    const onCall = () => {
        try {
            const formattedArgs = JSON.parse(args) as unknown as any[]
            console.log('call meth with: ', formattedArgs, ...formattedArgs)

            meth.call(...formattedArgs).then(v => {
                setRetVal(v)
            }).catch(e => {
                setError(e)
            })
        } catch (e) {
            setError(e)
        }
    }

    return <Card style={{backgroundColor: "#fff2e6"}}>
        <Card.Header>
            [Method] {meth.getName()}
        </Card.Header>
        <Card.Body>
            <Row>
                <Col>
                    <Alert variant={"info"}>
                        Enter method arguments as a Json array. Example for one string arg: ["foo"]
                    </Alert>
                    <Form.Control onChange={(e) => setArgs(e.target.value)} value={args} type="text" placeholder="Arguments"/>
                </Col>

                <Col>
                    <Button onClick={onCall} className="mr-3">Call with args</Button>
                    {
                        error && <label className="text-warning">{JSON.stringify(error)}</label>
                    }
                    {
                        retVal && <label className="text-success">{JSON.stringify(retVal)}</label>
                    }
                </Col>
            </Row>
        </Card.Body>
    </Card>
}
