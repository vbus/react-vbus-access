import React from "react";
import {NodeProxy} from "@veea/vbus-access";
import {Accordion, Card} from "react-bootstrap";
import Element from "./Element";


/**
 * Render a vBus node.
 */
export default function Node({node}: { node: NodeProxy }) {
    const elements = node.elements()

    return <Accordion defaultActiveKey={node.getPath()}>
        <Card>
            <Accordion.Toggle as={Card.Header} eventKey={node.getPath()}>
                {node.getName()}
            </Accordion.Toggle>
            <Accordion.Collapse eventKey={node.getPath()}>
                <Card.Body>
                    {
                        Object.keys(elements).map((uuid, i) => {
                            const element = elements[uuid]
                            return <Element element={element}/>
                        })
                    }
                </Card.Body>
            </Accordion.Collapse>
        </Card>
    </Accordion>
}
