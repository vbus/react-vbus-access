import React, {useState} from "react";
import {AttributeProxy, UnknownProxy} from "@veea/vbus-access";
import {Card, Button} from "react-bootstrap";


/**
 * Render a vBus attribute.
 */
export default function Attribute({attr}: { attr: AttributeProxy }) {
    const [value, setValue] = useState(null)
    const [error, setError] = useState(null)

    const onRead = () => {
        attr.readValue().then(v => {
            setValue(v)
        }).catch(e => {
            setError(e)
        })
    }

    return <Card style={{backgroundColor: "#eaf5db"}}>
        <Card.Header>
            [Attribute] {attr.getName()}
        </Card.Header>
        <Card.Body>
            <Button onClick={onRead} className="mr-3">Read</Button>
            {
                error && <label className="text-warning">{JSON.stringify(error)}</label>
            }
            {
                value && <label className="text-success">{JSON.stringify(value)}</label>
            }
        </Card.Body>
    </Card>
}
