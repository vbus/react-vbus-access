import React from "react";
import {useDiscover} from "@veea/react-vbus-access";
import {ModuleInfo} from "@veea/vbus-access";
import Element from "./Element";


interface Props {
    module: ModuleInfo
}

/**
 * Display module details.
 */
export default function ModuleDetail({module}: Props) {
    const {proxy, error} = useDiscover(module.id, 1)

    console.log(proxy, error)

    if (!proxy && !error) {
        return <label>loading...</label>
    }

    if (error) {
        return <label>Discover error: {JSON.stringify(error)}</label>
    }

    return <Element element={proxy}/>
}
