import React from "react";
import {UnknownProxy} from "@veea/vbus-access";
import Node from "./Node";
import Attribute from "./Attribute";
import Method from "./Method";


/**
 * Render a generic vBus element.
 */
export default function Element({element}: { element: UnknownProxy }) {
    if (element.isNode()) {
        return <Node node={element.asNode()}/>
    } else if (element.isAttribute()) {
        return <Attribute attr={element.asAttribute()}/>
    } else if (element.isMethod()) {
        return <Method meth={element.asMethod()}/>
    } else {
        return <label>Unknown element</label>
    }
}

