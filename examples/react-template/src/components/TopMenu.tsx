import React from "react";
import {Nav, Navbar, NavDropdown, Form, FormControl, Button} from "react-bootstrap"

export default function TopMenu() {
    return (
        <Navbar bg="light" expand="lg">
            <Navbar.Brand href="#home">React-vbus-access Template Project</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                    <Nav.Link href="home">Home</Nav.Link>
                    <Nav.Link href="modules">Modules</Nav.Link>
                </Nav>
                <Form inline>
                    <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                    <Button variant="outline-success">Search</Button>
                </Form>
            </Navbar.Collapse>
        </Navbar>
    )
}
