import React, {useEffect} from "react";
import {Redirect} from "react-router-dom";
import {withAuthentication} from "@veea/react-vbus-access"



/**
 * This is a wrapper on `withAuthentication` to set a default action when not authenticated.
 * In this case: we do a top level redirect (because we are in an iframe) to the dashboard login screen.
 */
const withMyAuth = (component: React.ComponentType) => withAuthentication(component, <NotAuthenticated/>, <div/>)

export default withMyAuth


function inIframe () {
    try {
        return window.self !== window.top;
    } catch (e) {
        return true;
    }
}

const NotAuthenticated = () => {
    useEffect(() => {
        if (inIframe()) {
            console.log("you are not authenticated")
            if (window.top) {
                window.top.location.href = window.top.origin + "/login"
            }
        }
    }, [])

    return <></>
}
