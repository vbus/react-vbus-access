import React from 'react';
import './App.css';
import HomePage from "./pages/HomePage";
import {VbusAccessContext, VbusRoute} from "@veea/react-vbus-access"
import {BrowserRouter} from "react-router-dom";
import ModulesPage from "./pages/ModulesPage";
import TopMenu from "./components/TopMenu";


export default function App() {
    return (
        <div className="App">
            <BrowserRouter>

                <TopMenu/>

                <VbusAccessContext>
                    <VbusRoute path="/modules" exact>
                        <ModulesPage/>
                    </VbusRoute>

                    <VbusRoute path="" exact>
                        <HomePage/>
                    </VbusRoute>
                </VbusAccessContext>
            </BrowserRouter>
        </div>
    );
}
