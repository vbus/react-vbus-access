import React, {useContext} from "react";
import withMyAuth from "../withMyAuth";
import {useHistory, useLocation} from "react-router-dom";
import {ApiContext} from "@veea/react-vbus-access";
import { Container } from "react-bootstrap";


function HomePage() {
    const location = useLocation()
    const history = useHistory()

    const client = useContext(ApiContext)
    if (client === null) {
        throw new Error("useAttribute should be used inside an ApiContext")
    }

    const onClick = () => {
        history.push("/done")
    }

    return (
        <Container className="mt-5">
            <p>
                This is a sample project to demonstrate how to create an UI for vBus with React.
            </p>

            <p>
                <a href="https://bitbucket.org/vbus/react-vbus-access">https://bitbucket.org/vbus/react-vbus-access</a>
            </p>

            <p>
                It also use <a href="https://react-bootstrap.github.io/">react-bootstrap</a> library for UI components.
            </p>

            <h5>
                Please click on 'Modules' menu to see how to use vBus elements.
            </h5>
        </Container>
    )
}


export default withMyAuth(HomePage)
