import React from "react";
import withMyAuth from "../withMyAuth";
import {Container, Alert, ListGroup, Card, Accordion} from "react-bootstrap";
import {useModules} from "@veea/react-vbus-access";
import ModuleDetail from "../components/ModuleDetail";


function ModulesPage() {
    const {modules, error} = useModules()

    return <Container className="mt-5">
        <Alert variant="success">
            This page demonstrate how to get running vBus modules and use vBus elements with React hooks.
        </Alert>

        <h5>
            vBus modules
        </h5>

        {
            modules &&
            <Accordion defaultActiveKey="0">
                {
                    modules.map((m, i) => (
                        <Card>
                            <Accordion.Toggle as={Card.Header} eventKey={i.toString()}>
                                {m.id}
                            </Accordion.Toggle>
                            <Accordion.Collapse eventKey={i.toString()}>
                                <Card.Body>
                                    <ModuleDetail module={m}/>
                                </Card.Body>
                            </Accordion.Collapse>
                        </Card>
                    ))
                }
            </Accordion>
        }

        {
            error && <label>Cannot load modules: {JSON.stringify(error)}</label>
        }

        {
            !modules && !error && <label>loading...</label>
        }
    </Container>
}


export default withMyAuth(ModulesPage)
